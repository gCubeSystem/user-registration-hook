package org.gcube.portal.removeaccount.thread;

import java.util.List;

import org.gcube.common.portal.PortalContext;
import org.gcube.portal.oidc.lr62.OIDCUmaUtil;
import org.gcube.vomanagement.usermanagement.model.GatewayRolesNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author Massimiliano Assante ISTI-CNR
 *
 */
public class RemovedUserAccountThread implements Runnable {

	private static final Logger _log = LoggerFactory.getLogger(RemovedUserAccountThread.class);

	final String SUBJECT = "Removed account notification";

	private String userNameToDelete;
	private String theAdminToken;
	private List<String> theAdminRolesString;

	

	public RemovedUserAccountThread(String userNameToDelete, String theAdminToken, List<String> theAdminRolesString) {
		super();
		this.userNameToDelete = userNameToDelete;
		this.theAdminRolesString = theAdminRolesString;
		this.theAdminToken = theAdminToken;
	}

	@Override
	public void run() {
        OIDCUmaUtil.provideConfiguredPortalClientUMATokenInThreadLocal("/" + PortalContext.getConfiguration().getInfrastructureName());
		try {
			_log.info("Trying to remove user " + userNameToDelete + " from JCR first, using storageHub with role: "+GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
			RemoveUserFromJCR rmJCR = new RemoveUserFromJCR(userNameToDelete, theAdminToken, theAdminRolesString);
			boolean result = rmJCR.remove();
			_log.info("The user " + userNameToDelete + " has been removed from JCR with success? " + result);
				
		} catch (Exception e) {
			_log.error("An error occurred during user workspace removal: ", e);
		}
	}
}
