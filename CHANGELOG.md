# Changelog for user-registration-hook

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.1.0]

- maven-parent 1.2.0
- maven-portal-bom 4.0.0
- StorageHub downstream components to upgrade in order to work with storagehub 1.5.0 [#27999]

## [v2.0.4] - 2021-05-25

- Updated to support new UMATokensProvider class [#21506]

## [v2.0.3] - 2021-04-12

- Just removed obsolete Home library deps from pom which were forgotten there in 6.8.0 release

## [v2.0.2] - 2020-11-13

- Remove user deletion from ldap upon remove account (offloaded to keycloak) [#20108]

## [v2.0.0] - 2017-11-29

- Provide user workspace drop API [#10483]
- Remove user from LDAP upon D4Science user account [#10484]

## [v1.1.0] - 2016-07-30

- Update for Liferay 6.2.5

## [v1.0.0] - 2015-06-30

- First release 
